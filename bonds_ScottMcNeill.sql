-- bond and assignments

--1.
SELECT * FROM bond WHERE CUSIP = '28717RH95'
--2.
SELECT * FROM bond ORDER BY maturity ASC
--3.
SELECT SUM(quantity * price) FROM bond
--4.
SELECT quantity * (coupon/100) from bond
--5.
SELECT * FROM bond where rating LIKE 'a%'
--6.
SELECT avg(price), avg(coupon/100), rating from bond GROUP BY rating
--7.
SELECT * from bond LEFT JOIN rating ON rating.rating = bond.rating where coupon/price < rating.expected_yield