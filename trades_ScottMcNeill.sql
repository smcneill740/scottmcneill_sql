--Trades
-- 1.
SELECT *
FROM trade t
JOIN position p on p.opening_trade_id = t.id OR p.closing_trade_id = t.id
WHERE t.stock = 'MRK' AND p.trader_id = 1

-- 2. 
SELECT sum(t.size * t.price * (CASE WHEN t.buy = 1 THEN -1 ELSE 1 END))
FROM trade t
JOIN position p on p.opening_trade_id = t.id OR p.closing_trade_id = t.id
WHERE p.trader_id = 1 AND t.stock = 'MRK' AND p.opening_trade_id != p.closing_trade_id AND p.closing_trade_id IS NOT NULL;

-- 3.
--CREATE VIEW ProfitLoss AS 